﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuizCore.classes
{
    class UserSearching
    {
        public UserSearching() { }

        public int Id;

        public string UserId;

        private bool _LoggedIn;
        public string LoggedIn
        {
            get
            {
                if (_LoggedIn)
                {
                    return "1";
                }
                else
                {
                    return "0";
                }
            }
            set
            {
                switch (value)
                {
                    case "0":
                        _LoggedIn = false;
                        break;
                    case "1":
                        _LoggedIn = true;
                        break;

                }
            }
        }

        #region conv
        //// User-defined conversion from string to bool
        //public static implicit operator bool(string d)
        //{
        //    switch (d)
        //    {
        //        case "0":
        //            return false;
        //        case "1":
        //            return true;
        //        default:
        //            throw new Exception("StringBoolConvFailed");
        //    }
        //}

        ////  User-defined conversion from bool to string
        //public static implicit operator string(bool d)
        //{
        //    switch (d)
        //    {
        //        case false:
        //            return "0";
        //        case true:
        //            return "1";
        //        default:
        //            throw new Exception("BoolStringConvFailed");
        //    }
        //}
        #endregion
    }
}
