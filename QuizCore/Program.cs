﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;

using System.Timers;
using QuizCore.classes;
using System.Data;
using System.Data.Linq;
using System.Net;

namespace QuizCore
{
    class Program
    {
        static SqlConnection sqlconn;
        static List<UserSearching> userSearching = new List<UserSearching>();
        static List<string> gameList = new List<string>();
        private static int GameCounter = 1;

        static void Main(string[] args)
        {
            Timer userSearchingTimer = new Timer();
            userSearchingTimer.Elapsed += new ElapsedEventHandler(UserSearchingTimerCallback);
            userSearchingTimer.Interval += 3500;
            userSearchingTimer.AutoReset = true;

            try
            {
                sqlconn = DbConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=Quiz;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
                Console.WriteLine("[DEBUG] DB Connection ready.");
                gameList.Add("Game"+GameCounter);

                sqlconn.Open();
                userSearchingTimer.Start();
                Console.WriteLine("[DEBUG] UserSearching Timer Started.");

            }
            catch (Exception e)
            {
                switch (e.Message)
                {
                    case "SqlConnFailed":
                        break;
                    case "":
                        break;
                }

                if (string.Format("There is already an object named '{0}' in the database.", gameList.Last()) == e.Message)
                {
                    
                }
            }


            do
            {
                switch (Console.ReadLine())
                {
                    case "games":
                        Console.WriteLine("Games:");
                        ListGames();
                        break;
                    case "quit":
                        Console.WriteLine("Shutting down...");
                        return;
                    case "h":
                    case "help":
                    default:
                        Console.WriteLine("Help List.\n'games' - list games\n'quit' - Shutdown the server.");
                        break;
                }
                
            } while (true);

        }

        private static void ListGames()
        {
            using (var dc = new DataContext(sqlconn))
            {
                List<string> temp = dc.ExecuteQuery<string>("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_NAME LIKE 'Game_';").ToList();
                foreach (var item in temp)
                {
                    Console.WriteLine(item);
                }
            }
        }

        private static void UserSearchingTimerCallback(object source, ElapsedEventArgs e)
        {
            try
            {
                using (var dc = new DataContext(sqlconn))
                {
                    userSearching = dc.ExecuteQuery<UserSearching>("SELECT * FROM dbo.UserSearching").ToList();
                    string running = dc.ExecuteQuery<string>(string.Format("SELECT Running FROM[dbo].[{0}]", gameList.Last())).Last();
                    if (running.Contains("1"))
                    {
                        GameListUpdater();
                        dc.ExecuteCommand(CreateGameUserTableCommText());
                        dc.ExecuteCommand(CreateGameTableCommText());
                        Console.WriteLine(String.Format("[DEBUG] New Game Created - {0}.",gameList.Last()));
                    }
                }
            }
            catch (Exception ex)
            {
                switch (ex.Message)
                {
                    case "Invalid object name 'dbo.Game1'.":
                        using (var dc = new DataContext(sqlconn))
                        {
                            dc.ExecuteCommand(CreateGameUserTableCommText());
                            dc.ExecuteCommand(CreateGameTableCommText());
                            dc.ExecuteCommand(String.Format("INSERT INTO [dbo][{0}] (Id,UserId)",gameList.Last()));
                            Console.WriteLine(String.Format("[DEBUG] New Game Created From Exception - {0}.",gameList.Last()));
                        }
                        break;
                }
            }

            if (userSearching.Count != 0)
            {
                try
                {
                    using (var dc = new DataContext(sqlconn))
                    {
                        foreach (UserSearching item in userSearching)
                        {
                            dc.ExecuteCommand(String.Format("INSERT INTO [dbo].[{0}Users] (Id, LoggedIn) VALUES ({1}, {2})", gameList.Last(), item.UserId,item.LoggedIn));
                            Console.WriteLine(String.Format("[DEBUG] User {0} to {1}.", item.UserId ,gameList.Last()));
                            dc.ExecuteCommand(String.Format("DELETE FROM [dbo].[UserSearching] WHERE [dbo].[UserSearching].UserId={0}", item.UserId));
                        }
                        //return String.Format("INSERT INTO [dbo].[{0}Users] (Id, LoggedIn) SELECT TOP (4) UserId, LoggedIn FROM [dbo].[UserSearching]",gameList.Last());
                        //dc.ExecuteCommand(MoveUserToGameTableCommText());
                    }
                }
                catch (Exception ex) //System.Data.SqlClient.SqlErrorCollection
                {
                    if (string.Format("There is already an object named '{0}' in the database.", gameList.Last()) == ex.Message)
                    {
                        
                    }
                    else
                    {
                        throw new Exception(ex.Message);
                    }
                }

                using (var dc = new DataContext(sqlconn))
                {
                    List<string> tempCount = dc.ExecuteQuery<string>(String.Format("SELECT Id FROM dbo.{0}Users",gameList.Last())).ToList();
                    if (tempCount.Count >= 4)
                    {
                        dc.ExecuteCommand(String.Format("UPDATE [dbo].[{0}] SET Running = '1'", gameList.Last()));
                        Console.WriteLine(String.Format("[DEBUG] Game {0} started.", gameList.Last()));
                    }
                }
            }

            Console.WriteLine("[DEBUG] Searcing for new player in 3.5s...");

            //GC.Collect();
        }

        private static SqlConnection DbConnection(string connectionString)
        {
            SqlConnection dbconn;

            try
            {
                return dbconn = new SqlConnection(connectionString);
            }
            catch (Exception e)
            {
                throw new Exception("SqlConnFailed");
            }
        }

        private static void GameListUpdater()
        {
            GameCounter++;
            gameList.Add("Game"+GameCounter);
        }

        private static string CreateGameTableCommText()
        {
            return String.Format("CREATE TABLE[dbo].[{0}] (Id INT NOT NULL PRIMARY KEY, Winner NCHAR(10) NULL, Running NCHAR(10) NOT NULL DEFAULT 0, GameStartedTime NCHAR(10) NULL)", gameList.Last());
        }
        private static string CreateGameUserTableCommText()
        {
            return String.Format("CREATE TABLE[dbo].[{0}Users] (Id NCHAR(10) NOT NULL PRIMARY KEY, LoggedIn NCHAR(1) NOT NULL, Score INT NOT NULL DEFAULT 0)", gameList.Last());
        }
        private static string MoveUserToGameTableCommText()
        {
            return String.Format("INSERT INTO [dbo].[{0}Users] (Id, LoggedIn) SELECT TOP (4) UserId, LoggedIn FROM [dbo].[UserSearching]",gameList.Last());
        }

        //CREATE TABLE[dbo].[Game1]
        //(

        //[Id] INT        NOT NULL,
        //[Winner] NCHAR(10) NULL,
        //[Running] NCHAR(10) NOT NULL DEFAULT 0,
        //[GameStartedTime] NCHAR(10) NULL, 
        //PRIMARY KEY CLUSTERED([Id] ASC)
        //    );

    }
}
